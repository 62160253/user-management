/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pin.usermanagement;

import java.io.Serializable;

/**
 *
 * @author USER
 *
 */
public class User implements Serializable{

    private String userName;
    private String password;

    public User(String useName, String password) {
        this.userName = useName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUseName(String useName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "useName=" + userName + ", password=" + password;
    }

   

}
